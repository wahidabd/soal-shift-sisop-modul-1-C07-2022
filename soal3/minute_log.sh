loc=~/log/

date=$(date +"%Y%m%d%H%M%S")

if [[ ! -d "$loc" ]]
then
	mkdir $loc
fi

mem=`free -m /home/wahid/ | awk '/Mem:/ {print $2","$3","$4","$5","$6","$7}'`
swap=`free -m /home/wahid/ | awk '/Swap:/ {print $2","$3","$4}'`
mon=`du -sh /home/wahid/ | awk '{print $2","$1}'`

header=`printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"`
echo -e "$header\n$mem, $swap, $mon" >> $loc/metrics_$date.log
