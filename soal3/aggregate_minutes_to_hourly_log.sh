#loc=~/log

timeNow=$(date +"%Y%m%d%H")
timeAgo=$(date -d '1 hour ago' +"%Y%m%d%H")

for (( i=0; i<=59; i++ ))
do
	if (( i >= 0 && i < 10 ))
	then
		check=~/log/metrics_$timeNow"0"$i"01".log
		if [[ ! -f "$check" ]]
		then
			cat ~/log/metrics_$timeNow"0"$i"02".log
		else
			cat $check
		fi
	else
		check=~/log/metrics_$timeNow$i"01".log
		if [[ ! -f "$check" ]]
		then
			cat ~/log/metrics_$timeAgo$i"02".log
		else
			cat $check
		fi
	fi
done | awk -F "," {print $1}
