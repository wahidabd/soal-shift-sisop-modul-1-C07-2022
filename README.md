# soal-shift-sisop-modul-1-C07-2022

## Anggota Kelompok ##

NRP | NAMA
------------- | -------------
5025201039  | Abd. Wahid
5025201056  | Rendi Dwi Francisko
5025201239  | Kadek Ari Dharmika

## Soal 1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

a. Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh

b. Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
    i. Minimal 8 karakter
    ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
    iii. Alphanumeric
    iv. Tidak boleh sama dengan username
    
c. Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
    -i. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
    -ii. Ketika percobaan register berhasil, maka message pada log adalah REGISTER : INFO User USERNAME registered successfully
    -iii. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
    -iv. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

d. Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
    -i. dl N ( N = Jumlah gambar yang akan didownload) 
        Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder
        dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).
        Setelah berhasildidownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila
        sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu,barulah mulai ditambahkan gambar yang baru, kemudian folder di zip
        kembali dengan password sesuai dengan user.
    -ii. att
        Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini. 
    
    
### Pada register.sh    

Mencatat tanggal dan jamnya dicatat
```
calendar=$(date +%D)
time=$(date +%T)
```
Lalu user memasukan input username serta password
```
printf "Enter your username: "
read username

printf "Enter yout password: "
read -s password
```

Setelah itu user dan password divalidasi dengan fungsi "func_check_password",dalam fungsi tersebut username dan password dicek agar sesuai dengan yang diminta pada soal
```
local lengthPassword=${#password}
local locFolder=/home/wahid/sisop/modul1
local locUsers=$locFolder/users

if [[ ! -d "$locUsers" ]]
    then
        mkdir $locUsers
    fi
```
Pada code diatas akan akan mencari tau panjang password dan lokasi dari folder yang akan digunakan untuk menyipan user yang akan mendaftar, lalu dicek apakah directory sudah ada atau belum, jika tidak ada maka akan dibuat dengan menjalanakan perintah mkdir

Cek apakah user sudah dibuat
```
 if grep -q $username "$locUser"
    then
        existsUser="User already exists"

        echo $existsUser
        echo $calendar $time REGISTER:ERROR $existsUser >> $locLog
```
jika username sudah ada pada $locUser maka username tersebut tidak valid sehingga tidak masuk sebagai username baru

Cek apakah password sama dengan username
```
 elif [[ $password == $username ]]
    then
        echo "Password cannot be the same as username"
```
jika password sama dengan username yang dimasukan maka username tersebut tidak valid 

Cek apakah password kurang dari 8 karakter
```
elif [[ $lengthPassword -lt 8 ]]
    then
        echo "Password must be more than 8 characters"
```
jika password kurang dari 8 karakter maka password tersebut tidak dapat digunakan sebagai password yang valid

Cek apakah password terdapat huruf kapital,huruf kecil,dan angka
```
elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
    then
        echo "Password must be at least upper, lower and number!"
```
Jika password tidak terdapat minimal satu huruf kapital,huruf kecil,dan angka maka password tersebut tidak valid

Username dan password valid
```
else
        echo "Register successfull!"
        echo REGISTER:INFO User $username registered successfully >> $locLog
        echo $calendar $time $username $password >> $locUser
```
Jika telah memenuhi semua kondisi maka user tersebut valid sehingga dapat dimasukan ke dalam $locUser




##### Dokumentasi jalannya program `register.sh`
![messageImage_1646552986339](/uploads/629cd9869b2e0ea0218045a2bd08a4d9/messageImage_1646552986339.jpg)

##### Mengecek apakah username dan password sudah tersimpan di file user.txt
![messageImage_1646553608756](/uploads/82f42d70cd1c3dd41c35689c1b9d9b25/messageImage_1646553608756.jpg)




### Pada main.sh
pada main.sh terdapat 2 command yaitu att dan dl yang dimana command att digunakan untuk menghitung percobaan login dan command dl digunakan untuk download gambar pada web https://loremflickr.com/320/240.

Inisiasi directory untuk menyimpan file hasil download
```
folder=$(date +%Y-%m-%d)_$username
locLog=/home/rendi/sisop/modul1/log.txt
locUser=/home/rendi/sisop/modul1/users/user.txt
```
nama folser tersebut adalah YYYY-MM-DD_username

Seperti yang terdapat pada register.sh,tanggal dan waktunya dicatat menggunakan
```
calendar=$(date +%D)
time=$(date +%T)
```
Setelah itu user memasukan username dan password untuk login
```
printf "Enter your username: "
read username

printf "Enter your password: "
read -s password
```
Setelah username dan password dimasukan maka akan dicek dengan fungsi "func_check_password"
```
func_check_password(){
        local lengthPassword=${#password}

        if [[ $lengthPassword -lt 8 ]]
        then
                echo "Password must be more than 8 characters"

        elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
        then
                echo "Password must be at least upper, lower and number!"

        else
                func_login
        fi
}
```
jika password kurang dari 8 karakter serta tidak memiliki minimal satu hurud kapital,huruf kecil,dan angka maka login akan gagal,jika tidak maka login berhasil dan akan masuk ke dalam fungsi "func_login"
```
func_login(){
        checkUser=$(egrep $username "$locUser")
        checkPass=$(egrep $password "$locUser")

        if [[ ! -f "$locUser" ]]
        then
                echo "no user registered yet"
        else
                if [[ -n "$checkUser" ]] && [[ -n "$checkPass" ]]
                then
                        echo "$calendar $time LOGIN:INFO User $username logged in" >> $locLog
                        echo "Login success"

                        printf "Enter command [dl or att]: "
                        read command
                        if [[ $command == att ]]
                        then
                                func_att
                        elif [[ $command == dl ]]
                        then
                                func_dl_pic
                        else
                                echo "Not found"
                        fi

                else
                        fail="Failed login attemp on user $username"
                        echo $fail

                        echo "$calendar $time LOGIN:ERROR $fail" >> $locLog
                fi
        fi
}
```
pada fungsi login akan dicek folder dan file user terlebih dahulu, jika file user.txt ditemukan maka akan mengecek username dan password akan dicek kembali apakah ada dalam $lockUser jika ada maka login berhasil dan jika tidak ada maka login gagal dan akan dicatat ke dalam $locLog.Pada saat login berhasil maka user akan disuruh memasukan 2 command yaitu att dan dl,jika memasukkan command att maka akan masuk ke dalam fungsi "func_att"
```
func_att(){
        awk -v user="$username" 'BEGIN {count=0} $5 == user || $9 == user {count++} END {print (count)}' $locLog
}
```
pada fungsi "func_att" akan menampilkan berapa kali percobaan login yang telah dilakukan oleh user yang login, kemudian akan dicatat dalam $locLog. jika user memasukan command dl maka akan masuk ke dalam fungsi "func_dl_pic"
```
func_dl_pic(){
        printf "Enter number: "
        read n

        if [[ ! -f "$folder.zip" ]]
        then
                mkdir $folder
                count=0
                func_start_dl
        else
                func_unzip
        fi

}
```
Pada fungsi tersebut user diminta memasukan jumlah gambar yang akan di download lalu dicek apakah $folder.zip sudah ada. $folder.zip merupakan nama file yang sesuai dengan tanggal,jika $folder.zip belum ada maka akan dibuatkan $folder.zip  countnya 0 dan akan mulai start download dengan menggunakan fungsi "func_start_dl" 
```
func_start_dl(){
        for(( i=$count+1; i<=$n+$count; i++ ))
        do
                wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
        done

        zip --password $password -r $folder.zip $folder/
        rm -rf $folder
}
```
Pada fungsi tersebut akan mulai download gambar dengan jumlah yang diminta dengan menggunakan wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg.Nama filenya .jpgnya sesuai dengan nama folder/PIC_(urutan gambar).jpg lalu foldernya di zip dan diberi password kemudian folder yang tidak memiliki format .zip akan dihapus.
Kembali pada fungsi "func_dl_pic" jika $folder.zip telah ada maka file tersebut akan di unzip terlebih dahulu dengan menggunakan fungsi "func_unzip"
```
func_unzip(){
        unzip -P $password $folder.zip
        rm $folder.zip

        count=$(find $folder -type f | wc -l)
        func_start_dl
}
```
Pada fungsi tersebut $folder.zip akan di unzip dengan menggunakan password user dan file .zip dihapus, lalu count akan dihitung dengan menghitung jumlah gambar yang telah di download, count ini digunakan untuk penamaan file gambarnya, lalu akan masuk ke dalam fungsi "func_start_dl".


##### Dokumentasi berjalannya program `main.sh`

- Menjalankan command `att`

![messageImage_1646559080977](/uploads/3418c5e20db703ec0536e3fd6b5d6105/messageImage_1646559080977.jpg)

- Menjalankan command `dl`

![messageImage_1646556663289](/uploads/b680c3bb55cc13a67517e024fe81bd23/messageImage_1646556663289.jpg)

### Kendala dan Kesulitan
- Pada saat mengecek username dan password
- Pada saat memberikan password pada file zip
- Pada saat menghitung jumlah file didalam folder image yang sudah ada


## Soal 2
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak
bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan.
Dapos langsung membuka log website dan menemukan banyak request yang berbahaya.
Bantulah Dapos untuk membaca log website https://daffa.info

### A
```
locFolder=/home/wahid/sisop/modul1
locLog=$locFolder/log_website_daffainfo.log
folder=$locFolder/forensic_log_website_daffainfo_log

#create dir
if [[ -d "$folder" ]]
then
        rm -rf $folder
        mkdir $folder
else
        mkdir $folder
fi
```

Dapat dilihat, pada locFolder adalah folder untuk menyimpan semua informasi yang nantinya akan dibutuhkan. Pada source code diatas akan dicek terlbih dahulu, apakah folder forensic_log_website_daffainfo_log sudah ada atau tidak, jika ada maka folder yang lama akan dihapus dan akan dibuatkan folder yang baru, jika folder tidak ada maka akan langsung dibuatkan folder baru.

### B
```
cat $locLog | awk -F: '{gsub(/"/, "", $3)
        arr[$3]++}
        END {
                for (i in arr) {
                        res+=arr[i]
                }
                res=res/12
                printf "rata rata serangan perjam adalah sebanyak %.3f request per jam\n\n", res
        }' >> $folder/ratarata.txt
```

Pada syntax cat $locLog akan dibaca request yang dikirim penyerang. Lalu dengan awk -F record dari request akan dibreakdown string-string yang dipisahkan dengan titik 2 (:) dan dengan gsub maka tanda double qoute dihapus. Variable dengan $3 jika dilihat dari urutan request yang dipisahkan dengan titik 2 (:) merupakan jam dari requestnya. Untuk mencatat jumlah request setiap jamnya
variabel $3 yang merupakan jamnya di tampung sebagai index di array 'Arr' dan setiap kali ditemukan request dengan jam tertentu, array 'Arr' akan ditambahkan jumlahnya yang menandakan jumlah request pada jam tersebut. Untuk menghitung rata-rata request setiap jamnya, rule END dibuat dimana terdapat for loop yang mengiterasikan setiap element yang ada di dalam array 'Arr'. Total index dalam array menunjukan total jam adanya request dari penyerang dan nilai dalam array akan ditampung dalam variabel res untuk menghitung total serangan yang terjadi pada website. Setelah loop selesai, variable res direassign dengan rata-rata jumlah serangan per jamnya dengan membagi nilai res atau total serangan di bagi dengan total jam adanya serangan yang ditampung pada variabel count. Selanjutnya, rata-rata serangan per jam yang ditampung pada variabel res dimasukan dalam file ratarata.txt.

### C
```

cat $locLog | awk -F: '{gsub(/"/, "", $1)
	arr[$1]++}
	END {
		max=0
		target
		for (i in arr){
			if (max < arr[i]){
				target = i
				max = arr[target]
			}
		}
		print "yang paling banyak mengakses server adalah: " target " sebanyak " max " request\n"
	}' >> $folder/result.txt
```

Pada awal syntax akan dibaca request yang dikirimkan kepada website dimana dengan awk -F akan dibreakdown string-string yang dipisahkan dengan titik dua (:). Dengan adanya command 'gsub', maka double qoute akan dihilangkan dari request yang ada di log. Jika dilihat dari request yang sudah dibreakdown dengan awk -F, lokasi IP terdapat pada variabel $1. Untuk menghitung jumlah request berdasarkan IP, maka IP nya akan ditampung sebagai index dari array 'Arr' dan jumlahnya akan ditambahkan saat program membaca adanya request dari IP tersebut. Untuk mencari IP yang paling banyak melakukan request, rule END dibuat dimana terdapat for loop yang mengiterasikan semua elemen dalam array 'Arr'. Variabel max akan menampung jumlah request paling banyak sementara dari IP dan variabel target untuk menamping IP mana yang melakukan request terbanyak. Dalam for loop, ada if condition yag akan mengganti IP dengan request terbanyak sementara jika terdapat IP dengan request yang lebih banyak lagi. Setelah iterasi untuk mencari IP dengan request terbanyak selesai, IP yang dengan request terbanyak dan jumlah dari requestnya akan dikirim ke dalam file 'result.txt'

### D
```

cat $locLog | awk '/curl/ {++n} END {
print "ada " n " request yang menggunakan curl sebagai user-agent\n"}' >> $folder/result.txt
```

Pertama dibaca terlebih dahulu baris request log yang ada dengan command 'cat'. Untuk mencari request dengan user-agent 'curl' dapat dicari dengan menggunakan awk dimana akan mencari line dengan key word 'curl'. Jika didapatkan request dengan key word 'curl' maka tampung jumlahnya di variabel n. Dengan rule END yang akan menjalankan syntax setelah semua input dari log dibaca, print total jumlah request dengan user-agent 'curl' yang ditampung dalam variabel n ke dalam file 'result.txt'

### E
```

cat $locLog | awk -F: '/2022:02/ {gsub(/"/, "", $1)
	arr[$1]++}
	END {
		for (i in arr){
			print i " Jam 2 Pagi"
		}
	}' >> $folder/result.txt
```

Pertama, dibaca terlebih dahulu baris request yang dikirimkan ke website dengan command 'cat'. Untuk mencari baris request pada jam 2 pagi, dapat dicari dengan awk dimana kriteria 2022:02. Selanjutnya dengan gsub yang menghilangkan double qoute dalam baris request di log, IP dengan request jam 2 pagi akan ditampung dalam array 'arr'. Setelah program selesai mengambil input dari log, maka dijalankan for loop untuk print semua request pada jam 2 pagi ke dalam 'file result.txt'


##### Dokumentasi berjalannya program `soal2_forensic_dapos.sh`
 
- Sebelum dan setelah berjalannya program

![messageImage_1646552198212](/uploads/310b87b31107f7fe9c9b944ab2ed1e45/messageImage_1646552198212.jpg)

- Isi file `ratarata.txt`

![messageImage_1646552275114](/uploads/cc3708503b36d04597785bbbdec4a1d2/messageImage_1646552275114.jpg)

- Isi file `result.txt` 

![messageImage_1646552302710](/uploads/192f21c68316fdd6e6047e14c808ce15/messageImage_1646552302710.jpg)


### Kendala dan Kesulitan
- Pada saat split :
- Pada saat mencari rata2 perjam


## Soal 3

Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer. Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/. 

a. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

b. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

c. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap
metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log

d. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

### Pada minute_log.sh

```
loc=~/log/

```

Variabel `loc` dideklarasikan untuk menyimpan hasil log yang akan dijalankan di dalam folder `/log`


```

date=$(date +"%Y%m%d%H%M%S")

if [[ ! -d "$loc" ]]
then
	mkdir $loc
fi

```

Variabel `date` dideklarasikan untuk menyimpan tanggal dan waktu dari berjalannya program. Untuk mengecek apakah folder untuk menyimpan log dari program sudah ada, `if statement` dibuat dimana jika belum terdapat directory di variabel `loc` maka akan dibuat directory dengan `mkdir $loc`

```

mem=`free -m /home/wahid/ | awk '/Mem:/ {print $2","$3","$4","$5","$6","$7}'`
swap=`free -m /home/wahid/ | awk '/Swap:/ {print $2","$3","$4}'`
mon=`du -sh /home/wahid/ | awk '{print $2","$1}'`

```

Hasil dari `free -m` yang memonitoring ram dari directory /home/wahid/ ditampung dalam variabel `mem` untuk memory nya dan `swap` untuk swap memory. Untuk monitoring size directory yakni dengan command `du -sh` disimpan dalam variabel `mon`.

```

header=`printf "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size"`
group=`echo $mem,$swap,$mon`
echo -e "$header\n$group" >> $loc/metrics_$date.log

```

Setelah semua metrics untuk ram dan size directory ditampung dalam masing-masing variabel, variabel-variabel tersebut digabungkan dalam variabel `group`. Selanjutnya variabel tersebut di tampung dalam file log yang disesuaikan dengan waktu dimana program itu dijalankan.

##### Dokumentasi Berjalannya Program `minute_log.sh`

![WhatsApp_Image_2022-03-06_at_21.04.22](/uploads/1afb706db36de1f5999f0ce2027c5ef1/WhatsApp_Image_2022-03-06_at_21.04.22.jpeg)


### Kendala dan Kesulitan
- Pada saat menjalankan cronjob
- Pada soal C kesulitan untuk mengambil nilai dari dalam file karena nama file yang tidak konsisten
- Pada soal D, karena belum selesainya soal C maka kami belum mengerjakan soal D.

